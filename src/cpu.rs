use crate::mem::MC;


#[repr(C)]
#[derive(Clone, Copy)]
struct Bytes {
    l: u8,  // lower byte
    h: u8   // higher byte
}

/// Some instructions, allow you to use
/// the registers A, B, C, D, E, H, & L as 16- bit registers
/// by pairing them up in the following manner: AF, BC, DE, & HL.
#[repr(C)]
union WordRegister {
    bytes: Bytes,
    word: u16
}

#[repr(C)]
pub struct CPU {
    // registers
    a: u8,
    f: u8,                         // Register F contains processor flags
    bc: WordRegister,
    de: WordRegister,
    hl: WordRegister,
    pc: u16,                       // program counter
    sp: u16,                       // stack pointer

    ime: u8,                       // interrupt master enable
}


impl CPU {
    pub fn new() -> Self {
        CPU {
            a: 0,
            f: 0,
            bc: WordRegister { word: 0 },
            de: WordRegister { word: 0 },
            hl: WordRegister { word: 0 },
            pc: 0x100,                      // first instruction address
            sp: 0xFFFE,                     // highest location of RAM
            ime: 1,
        }
    }

    fn step(&mut self) -> () {
        let opcode: u8 = MC::read(self.pc);
        self.pc += 1;

        match opcode {
            0x06 => {

            },
            _ => println!("Undefined opcode")
        }
    }
}